# GitLabCI

This is how we test it. This is how we test it.

### Usage

Minimal tests:

```yml
include:
  - project: JuliaDiffEq/GitLabCI
    ref: master
    file: CI.yml
```

With a GPU group (CuArrays will be preinstalled):

```yml
include:
  - project: JuliaDiffEq/GitLabCI
    ref: master
    file: CI.yml
gpu:
  extends: .test
  tags:
    - p6000
  variables:
    GROUP: GPU
```

On ARM64:

```yml
include:
  - project: JuliaDiffEq/GitLabCI
    ref: master
    file: CI.yml
arm:
  extends: .test
  tags:
    - aarch64
```

Custom Julia version and thread count (make sure to use `major.minor.patch`):

```yml
include:
  - project: JuliaDiffEq/GitLabCI
    ref: master
    file: CI.yml
lts:
  extends: .test
  variables:
    JULIA_VERSION: 1.0.5
    JULIA_NUM_THREADS: 2
```

### Maintenance

In `CI.yml`, keep `JULIA_VERSION` set to the most recent Julia release.
